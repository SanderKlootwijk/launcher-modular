# Launcher modular

<a href="https://translate-ut.org/engage/launcher-modular/?utm_source=widget">
<img src="https://translate-ut.org/widgets/launcher-modular/-/translate/svg-badge.svg" alt="État de la traduction" />
</a>

A launcher modular for ubuntu touch

![home](https://i0.wp.com/ubuntu-touch-fr.org/wp-content/uploads/2019/05/screenshot20190410_150836013.png?resize=169%2C300&ssl=1)
![Agenda](https://i2.wp.com/ubuntu-touch-fr.org/wp-content/uploads/2019/05/screenshot20190410_150829986.png?resize=169%2C300&ssl=1)
![home](https://i1.wp.com/ubuntu-touch-fr.org/wp-content/uploads/2019/05/screenshot20190410_150821342.png?resize=169%2C300&ssl=1)

### How to build

To build Launcher Modular for Ubuntu Touch devices you do need clickable. Follow install instructions at its repo [here](https://gitlab.com/clickable/clickable). For more information and help visit the [docs](ble.bhdouglass.com/en/latest/getting-started.html).
Once clickable is installed you are ready to go.

1. Fork [Launcher Modular at gitlab](https://framagit.org/ubuntouch-fr-dev/launcher-modular) into your own namespace. You may need to open an accout with gitlab if not already done.

2. Open a terminal: ctl + alt + t.

3. Clone the repo onto your local machine using git: `git clone https://framagit.org/ubuntouch-fr-dev/launcher-modular.git`.

4. Change into Launcher Modular folder: `cd launcher-modular`.

5. run clickable: `clickable`.

### Translation

Goto [Launcher Modular on weblate](https://translate-ut.org/projects/launcher-modular/translate/), login, start translating.


[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/launchermodular.ubuntouchfr)